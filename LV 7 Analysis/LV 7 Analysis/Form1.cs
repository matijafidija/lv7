﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_7_Analysis
{
    public partial class formTicTacToe : Form
    {
        formNewGame NewGame;
        bool current = false;
        public formTicTacToe()
        {
            InitializeComponent();   
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bNewGame_Click(object sender, EventArgs e)
        {
            NewGame = new formNewGame(this);
            bNewGame.Enabled = false;
            NewGame.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox1.ImageLocation = "X.png";
                pictureBox1.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox1.ImageLocation = "O.png";
                pictureBox1.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox2.ImageLocation = "X.png";
                pictureBox2.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox2.ImageLocation = "O.png";
                pictureBox2.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox3.ImageLocation = "X.png";
                pictureBox3.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox3.ImageLocation = "O.png";
                pictureBox3.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox4.ImageLocation = "X.png";
                pictureBox4.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox4.ImageLocation = "O.png";
                pictureBox4.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox5.ImageLocation = "X.png";
                pictureBox5.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox5.ImageLocation = "O.png";
                pictureBox5.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox6.ImageLocation = "X.png";
                pictureBox6.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox6.ImageLocation = "O.png";
                pictureBox6.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox7.ImageLocation = "X.png";
                pictureBox7.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox7.ImageLocation = "O.png";
                pictureBox7.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox8.ImageLocation = "X.png";
                pictureBox8.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox8.ImageLocation = "O.png";
                pictureBox8.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            if (current == false)
            {
                pictureBox9.ImageLocation = "X.png";
                pictureBox9.Enabled = false;
                lblTurn.Text = lblP2Name.Text.ToString() + "'s turn";
            }
            else
            {
                pictureBox9.ImageLocation = "O.png";
                pictureBox9.Enabled = false;
                lblTurn.Text = lblP1Name.Text.ToString() + "'s turn";
            }
            Check();
            current = !current;
        }
        private void Check()
        {
            if(((pictureBox1.ImageLocation == pictureBox2.ImageLocation) && (pictureBox2.ImageLocation == pictureBox3.ImageLocation)) && pictureBox1.ImageLocation != "" && pictureBox2.ImageLocation != "" && pictureBox3.ImageLocation != "")
            {
                if (current == false) P1win();
                else P2win();
            }
            else if (((pictureBox4.ImageLocation == pictureBox5.ImageLocation) && (pictureBox5.ImageLocation == pictureBox6.ImageLocation)) && pictureBox4.ImageLocation != "" && pictureBox5.ImageLocation != "" && pictureBox6.ImageLocation != "")
            {
                if (current == false) P1win();
                else P2win();
            }
            else if (((pictureBox7.ImageLocation == pictureBox8.ImageLocation) && (pictureBox8.ImageLocation == pictureBox9.ImageLocation)) && pictureBox7.ImageLocation != "" && pictureBox8.ImageLocation != "" && pictureBox9.ImageLocation != "")
            {
                if (current == false) P1win();
                else P2win();
            }
            else if (((pictureBox1.ImageLocation == pictureBox4.ImageLocation) && (pictureBox4.ImageLocation == pictureBox7.ImageLocation)) && pictureBox1.ImageLocation != "" && pictureBox4.ImageLocation != "" && pictureBox7.ImageLocation != "")
            {
                if (current == false) P1win();
                else P2win();
            }
            else if (((pictureBox2.ImageLocation == pictureBox5.ImageLocation) && (pictureBox5.ImageLocation == pictureBox8.ImageLocation)) && pictureBox2.ImageLocation != "" && pictureBox5.ImageLocation != "" && pictureBox8.ImageLocation != "")
            {
                if (current == false) P1win();
                else P2win();
            }
            else if (((pictureBox3.ImageLocation == pictureBox6.ImageLocation) && (pictureBox6.ImageLocation == pictureBox9.ImageLocation)) && pictureBox3.ImageLocation != "" && pictureBox6.ImageLocation != "" && pictureBox9.ImageLocation != "")
            {
                if (current == false) P1win();
                else P2win();
            }
            else if (((pictureBox1.ImageLocation == pictureBox5.ImageLocation) && (pictureBox5.ImageLocation == pictureBox9.ImageLocation)) && pictureBox1.ImageLocation != "" && pictureBox5.ImageLocation != "" && pictureBox9.ImageLocation != "")
            {
                if (current == false) P1win();
                else P2win();
            }
            else if (((pictureBox3.ImageLocation == pictureBox5.ImageLocation) && (pictureBox5.ImageLocation == pictureBox7.ImageLocation)) && pictureBox3.ImageLocation != "" && pictureBox5.ImageLocation != "" && pictureBox7.ImageLocation != "")
            {
                if (current == false) P1win();
                else P2win();
            }
            else if (pictureBox1.ImageLocation != "" && pictureBox2.ImageLocation != "" && pictureBox3.ImageLocation != "" && pictureBox4.ImageLocation != "" && pictureBox5.ImageLocation != ""
                    && pictureBox6.ImageLocation != "" && pictureBox7.ImageLocation != "" && pictureBox8.ImageLocation != "" && pictureBox9.ImageLocation != "")
            {
                Draw();
            }
        }

        private void P1win()
        {
            string s = lblP1Score.Text;
            lblP1Score.Text = (int.Parse(s) + 1).ToString();
            MessageBox.Show(lblP1Name.Text + " wins!");
            Reset();
        }

        private void P2win()
        {
            string s = lblP2Score.Text;
            lblP2Score.Text = (int.Parse(s) + 1).ToString();
            MessageBox.Show(lblP2Name.Text + " wins!");
            Reset();
        }

        private void Draw()
        {
            MessageBox.Show("Draw!");
            Reset();
        }

        private void Reset()
        {
            pictureBox1.Enabled = true;
            pictureBox1.ImageLocation = "";
            pictureBox2.Enabled = true;
            pictureBox2.ImageLocation = "";
            pictureBox3.Enabled = true;
            pictureBox3.ImageLocation = "";
            pictureBox4.Enabled = true;
            pictureBox4.ImageLocation = "";
            pictureBox5.Enabled = true;
            pictureBox5.ImageLocation = "";
            pictureBox6.Enabled = true;
            pictureBox6.ImageLocation = "";
            pictureBox7.Enabled = true;
            pictureBox7.ImageLocation = "";
            pictureBox8.Enabled = true;
            pictureBox8.ImageLocation = "";
            pictureBox9.Enabled = true;
            pictureBox9.ImageLocation = "";
        }
    }
}
